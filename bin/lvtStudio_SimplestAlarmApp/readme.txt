Simplest Alarm App v1.0 - Created by lvtStudio 14/06/2018
A final project for the course. La Van Tien - 15520878 - SE330.I21.PMCL - UIT - VNU HCM City - Viet Nam.

- This application is for:
	+ One who fall as sleep during intense gaming session or botting/hanging, and need something to wake him up later.

- Compile notes:
	+ Because Windows' Direct3D conflicts with Java2D renderer,
		please add '-Dsun.java2d.d3d=false' when compile this application.