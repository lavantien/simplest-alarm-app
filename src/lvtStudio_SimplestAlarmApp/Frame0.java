package lvtStudio_SimplestAlarmApp;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Frame0 {

	private StringBuilder inputStr;
	private int hourSet;
	private int minuteSet;
	private boolean isEnable;
	private boolean isPlayingSound;

	private JFrame frame;
	private JLabel lblCurrentAlarm;
	private JTextField textField1;
	private JLabel lblSetAlarm;
	private JButton btnTurnOff;
	private JLabel lblCurrentTime;
	private JTextField textHeader;
	private JLabel lblShowTime;
	private JLabel lblShowAlarm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame0 window = new Frame0();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void processing() {
		PlaybackWAV playback = new PlaybackWAV();
		
		Thread clock = new Thread() {
			public void run() {
				try {
					for (;;) {
						Calendar cal = new GregorianCalendar();
						int second = cal.get(Calendar.SECOND);
						int minute = cal.get(Calendar.MINUTE);
						int hour = cal.get(Calendar.HOUR_OF_DAY);
						int day = cal.get(Calendar.DAY_OF_MONTH);
						int month = cal.get(Calendar.MONTH) + 1;
						int year = cal.get(Calendar.YEAR);
						int cday = cal.get(Calendar.DAY_OF_WEEK);
						
						lblShowTime.setText(String.format("%d:%d:%d - T%d, %d/%d/%d", hour, minute, second, cday, day, month, year));
						
						if (hour == hourSet && minute == minuteSet && isEnable) {
							lblShowAlarm.setForeground(new Color(255, 0, 0));
							lblShowAlarm.setText("Time up!");
							if (!isPlayingSound) {
								playback.playSound("/lvtStudio_SimplestAlarmApp/AlarmSound0.wav");
								isPlayingSound = true;
							}
						}
						if (!isEnable) {
							playback.stopSound();
							isPlayingSound = false;
						}
						
						sleep(1000);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		clock.start();
		
		Thread watch = new Thread() {
			public void run() {
				try {
					for (;;) {
						
						sleep(1000);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		watch.start();
	}
	
	/**
	 * Create the application.
	 */
	public Frame0() {
		initialize();
		processing();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(new Color(255, 204, 204));
		frame.setBounds(100, 100, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Simplest Alarm App v1.0");
		
		JButton btnSet = new JButton("Set");
		btnSet.setToolTipText("Set the alarm.");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//JOptionPane.showMessageDialog(null, "La V\u0103n Ti\u1EBFn - 15520878 - SE330.I21.PMCL");
				inputStr = new StringBuilder(textField1.getText());
				if (inputStr.length() != 5) {
					isEnable = false;
					lblShowAlarm.setForeground(new Color(255, 0, 0));
					lblShowAlarm.setText("Wrong format when set alarm!");
				} else if (Character.isDigit(inputStr.charAt(0)) && Character.isDigit(inputStr.charAt(1)) && Character.isDigit(inputStr.charAt(3))
						&& Character.isDigit(inputStr.charAt(4)) && inputStr.charAt(2) == ':') {
					int th = Integer.parseInt(inputStr.substring(0, 2));
					int tm = Integer.parseInt(inputStr.substring(3, 5));
					if ((th >= 0 && th <= 23) && (tm >= 0 && tm <= 59)) {
						hourSet = th;
						minuteSet = tm;
						isEnable = true;
						lblShowAlarm.setForeground(new Color(0, 0, 0));
						lblShowAlarm.setText(inputStr.toString());
					} else {
						isEnable = false;
						lblShowAlarm.setForeground(new Color(255, 0, 0));
						lblShowAlarm.setText("Wrong format when set alarm!");
					}
				} else {
					isEnable = false;
					lblShowAlarm.setForeground(new Color(255, 0, 0));
					lblShowAlarm.setText("Wrong format when set alarm!");
				}
			}
		});
		btnSet.setForeground(new Color(0, 0, 128));
		btnSet.setBackground(new Color(220, 220, 220));
		btnSet.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnSet.setBounds(117, 388, 123, 50);
		frame.getContentPane().add(btnSet);
		
		lblCurrentAlarm = new JLabel("Current Alarm and Prompt:");
		lblCurrentAlarm.setToolTipText("Show alarm and prompt about events.");
		lblCurrentAlarm.setVerticalAlignment(SwingConstants.BOTTOM);
		lblCurrentAlarm.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentAlarm.setForeground(new Color(0, 0, 128));
		lblCurrentAlarm.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCurrentAlarm.setBounds(10, 286, 474, 30);
		frame.getContentPane().add(lblCurrentAlarm);
		
		textField1 = new JTextField();
		textField1.setHorizontalAlignment(SwingConstants.CENTER);
		textField1.setFont(new Font("Tahoma", Font.BOLD, 30));
		textField1.setToolTipText("Enter time to alarm here, 24 hours format. Cannot accept illegal time.");
		textField1.setBounds(10, 225, 474, 50);
		frame.getContentPane().add(textField1);
		textField1.setColumns(10);
		
		lblSetAlarm = new JLabel("Set Alarm (hh:mm):");
		lblSetAlarm.setVerticalAlignment(SwingConstants.BOTTOM);
		lblSetAlarm.setHorizontalAlignment(SwingConstants.CENTER);
		lblSetAlarm.setToolTipText("Set alarm with 24 hours format.");
		lblSetAlarm.setForeground(new Color(0, 0, 128));
		lblSetAlarm.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblSetAlarm.setBounds(10, 184, 474, 30);
		frame.getContentPane().add(lblSetAlarm);
		
		btnTurnOff = new JButton("Turn off");
		btnTurnOff.setToolTipText("Turn off the alarm.");
		btnTurnOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isEnable = false;
				inputStr.setLength(0);
				lblShowAlarm.setText("");
				textField1.setText("");
			}
		});
		btnTurnOff.setForeground(new Color(0, 0, 128));
		btnTurnOff.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnTurnOff.setBackground(new Color(220, 220, 220));
		btnTurnOff.setBounds(250, 388, 123, 50);
		frame.getContentPane().add(btnTurnOff);
		
		lblCurrentTime = new JLabel("Current Time (hh:mm:ss - \"T\"dow, dd/mm/yyyy):");
		lblCurrentTime.setToolTipText("Current time and date, as formated.");
		lblCurrentTime.setVerticalAlignment(SwingConstants.BOTTOM);
		lblCurrentTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentTime.setForeground(new Color(0, 0, 128));
		lblCurrentTime.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCurrentTime.setBounds(10, 82, 474, 30);
		frame.getContentPane().add(lblCurrentTime);
		
		textHeader = new JTextField();
		textHeader.setBackground(new Color(255, 204, 204));
		textHeader.setHorizontalAlignment(SwingConstants.CENTER);
		textHeader.setForeground(new Color(255, 0, 0));
		textHeader.setFont(new Font("Tahoma", Font.BOLD, 22));
		textHeader.setText("Simplest Alarm App  -  lvtStudio 2018");
		textHeader.setToolTipText("A final project for the course. La V\u0103n Ti\u1EBFn - 15520878 - SE330.I21.PMCL - UIT - VNU HCM City - Viet Nam.");
		textHeader.setEditable(false);
		textHeader.setBounds(10, 11, 474, 60);
		frame.getContentPane().add(textHeader);
		textHeader.setColumns(10);
		
		lblShowTime = new JLabel("");
		lblShowTime.setToolTipText("Current time and date, as formated.");
		lblShowTime.setHorizontalAlignment(SwingConstants.CENTER);
		lblShowTime.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblShowTime.setBounds(10, 123, 474, 50);
		frame.getContentPane().add(lblShowTime);
		
		lblShowAlarm = new JLabel("");
		lblShowAlarm.setToolTipText("Show alarm and prompt about events.");
		lblShowAlarm.setHorizontalAlignment(SwingConstants.CENTER);
		lblShowAlarm.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblShowAlarm.setBounds(10, 327, 474, 50);
		frame.getContentPane().add(lblShowAlarm);
	}
}
